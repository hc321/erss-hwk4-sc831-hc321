#! /bin/bash

TEST_FILE1=test1.xml
TEST_FILE2=test2.xml
TEST_FILE3=test3.xml

# adjust the following number to change loop numbers
NUM_LOOP=100

./test $TEST_FILE1

for ((i = 0; i < $NUM_LOOP; ++i))
do
	./test $TEST_FILE2 &
	./test $TEST_FILE3 &
done
