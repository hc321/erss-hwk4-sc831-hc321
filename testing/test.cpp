#include <iostream>
#include <cstring>
#include <sys/socket.h>
#include <netdb.h>
#include <unistd.h>
#include <sys/socket.h>
#include <time.h> 
#include <unistd.h>
#include <chrono>
#include <ctime>
#include <random>
#include <ratio>
#include <sstream>
#include <string>
#include <thread>
#include <vector>
#include <arpa/inet.h>
#include <fcntl.h>
#include <fstream>
#include <netinet/in.h>
#include <pthread.h>
#include <sstream>
#include <stdlib.h>
#include <string.h>
#include <sys/select.h>
#include <sys/types.h>
#include <time.h>


using namespace std;
using namespace std::chrono;

#define SERVER_HOSTNAME "127.0.0.1"
#define SERVER_PORT "12345"
#define NUM_THREADS 10

high_resolution_clock::time_point start_t;
high_resolution_clock::time_point end_t;


string read_xml(string f){
  ifstream ifs;
  string line;
  string lines;
  ifs.open(f.c_str());
  if (!ifs.good()){
    cerr << "file open error"<<endl;
    EXIT_FAILURE;
  }
  while(getline(ifs,line)){
    lines.append(line);
  }
  ifs.close();
  return lines;
}

int send_xml(string f)
{
  int status;
  int socket_fd;
  struct addrinfo host_info;
  struct addrinfo *host_info_list;

  memset(&host_info, 0, sizeof(host_info));
  host_info.ai_family   = AF_UNSPEC;
  host_info.ai_socktype = SOCK_STREAM;

  status = getaddrinfo(SERVER_HOSTNAME, SERVER_PORT, &host_info, &host_info_list);
  if (status != 0) {
    cerr << "Error: cannot get address info for host" << endl;
    cerr << "  (" << SERVER_HOSTNAME << "," << SERVER_PORT << ")" << endl;
    return -1;
  } 

  socket_fd = socket(host_info_list->ai_family, 
        host_info_list->ai_socktype, 
        host_info_list->ai_protocol);
  if (socket_fd == -1) {
    cerr << "Error: cannot create socket" << endl;
    cerr << "  (" << SERVER_HOSTNAME << "," << SERVER_PORT << ")" << endl;
    return -1;
  } 
  
  
  status = connect(socket_fd, host_info_list->ai_addr, host_info_list->ai_addrlen);
  if (status == -1) {
    cerr << "Error: cannot connect to socket" << endl;
    cerr << "  (" << SERVER_HOSTNAME << "," << SERVER_PORT << ")" << endl;
    return -1;
  } 

  string data;
  string message;

  data = read_xml(f);
  message = to_string(data.length())+"\n" + data;

  send(socket_fd, message.c_str(), message.length(), 0);
  char buffer[51240];
  memset(&buffer, '\0', sizeof(buffer));
  recv(socket_fd, buffer, 51240, 0);
  cout << buffer;
  
  freeaddrinfo(host_info_list);
  close(socket_fd);

  return 0;
}

void* send_xml_wrapper(void* arg) {
  string* filename_ptr = static_cast<string*>(arg);
  string filename = *filename_ptr;
  send_xml(filename);
  return NULL;
}



int main(int argc, char **argv) {
  vector<pthread_t> threads;
  string str=argv[1];
  start_t= high_resolution_clock::now();
  for (int i = 0; i < NUM_THREADS; i++) {
    pthread_t thread;
     pthread_create(&thread, NULL, send_xml_wrapper, static_cast<void*>(&str));
    threads.push_back(thread);
  }
  for (int i = 0; i < threads.size(); i++) {
    pthread_join(threads[i], NULL);
  }
  end_t = high_resolution_clock::now();
  auto elapsed = std::chrono::duration_cast<std::chrono::milliseconds>(end_t - start_t);
  cout<<"time: "<<elapsed.count()<<" milliseconds."<<endl;
  return 0;
}