#include "createTables.h"

void initial_tables_construct(connection *C, string f){
  ifstream ifs;
  string line;
  string lines;
  ifs.open(f.c_str());
  if (!ifs.good()){
    // throw exception with error message
    throw runtime_error("Error: file not found");
  }
  while(getline(ifs,line)){
    lines.append(line);
  }
  ifs.close();
  work W(*C);
  W.exec(lines);
  W.commit();
}


int connect_and_creat() {
  //Allocate & initialize a Postgres connection object
  connection *C;

  try{
    //Establish a connection to the database
    //Parameters: database name, user name, user password
    C = new connection("dbname=STOCK_EXCHANGING_MATCHING user=postgres password=passw0rd");
    if (C->is_open()) {
    //  cout << "Opened database successfully: " << C->dbname() << endl;
    } else {
      cout << "Can't open database" << endl;
      return 1;
    }
  } catch (const std::exception &e){
    cerr << e.what() << std::endl;
    return 1;
  }
  try{
    initial_tables_construct(C,"database/initial_tables.sql");
  }
  catch (const std::exception &e){
    cerr << e.what() << std::endl;
    return 1;
  }
  C->disconnect();
  delete C;
  std::cout << "Tables created successfully" << std::endl;
  return 0;
}


