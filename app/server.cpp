#include <iostream>     
#include <string>
#include <sstream>
#include "handle_connection.hpp"
#include "utils.hpp"
std::mutex global_mutex;

int main(int argc, char * argv[]) {
    // write a server to continuously receive xml data from client
    while (true) {
        // Check command line arguments.
        // The arguments are: address, port,  number of threads
        if (argc != 4) {
        std::cerr << "Usage: http-server-async <address> <port> <threads>\n"
                    << "Example:\n"
                    << "    http-server-async 0.0.0.0 12345 1\n";
        return EXIT_FAILURE;
        }   
        // // run the database, create the table
        // int result = system("./database/createTables");
        // if (result == 0) {
        //     std::cout << "Program executed successfully.\n";
        // } else {
        //     std::cout << "Error: Program execution failed with code " << result << ".\n";
        //     return EXIT_FAILURE;
        // }
        // listen to the client and receive the xml data
        auto const address = net::ip::make_address(argv[1]);
        auto const port = static_cast<unsigned short>(std::atoi(argv[2]));
        auto const threads = std::max<int>(1, std::atoi(argv[3]));

        // The io_context is required for all I/O
        net::io_context ioc{threads};

        // Create and launch a listening port
        std::make_shared<listener>(
            ioc,
            tcp::endpoint{address, port},
            global_mutex
        )->run();

        // Run the I/O service on the requested number of threads
        std::vector<std::thread> v;
        v.reserve(threads - 1);
        for(auto i = threads - 1; i > 0; --i)
            v.emplace_back(
            [&ioc]
            {
                ioc.run();
            });
        ioc.run();
    }
    
    return EXIT_SUCCESS;    
}




