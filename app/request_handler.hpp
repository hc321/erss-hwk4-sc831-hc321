#pragma once
#include "utils.hpp"
#include "db_handler.hpp"

class request_handler
{
private:
    std::string request_;
    std::mutex & my_mutex_;
    pugi::xml_document req_;
    pugi::xml_document res_;
    pugi::xml_node result_node;
    std::string xml_to_string(pugi::xml_document & doc){
        std::stringstream ss;
        doc.save(ss);
        return ss.str();
    }

public:
    request_handler(std::string & request, std::mutex & my_mutex)
        : request_(request), my_mutex_(my_mutex)
    {
        result_node = res_.append_child("results");
        std::cout << "Received XML:\n" << request_ << std::endl;
    }
    std::string generate_response(){
        pugi::xml_parse_result result = req_.load_string(request_.c_str());
        if (!result){
            return invalid_request_response();
        }
        if (req_.first_child().name() == std::string("create")){
            return create();
        }
        else if (req_.first_child().name() == std::string("transactions")){
            return transactions();
        }
        return invalid_request_response();
    }
    std::string create(){
        pugi::xml_node create_node = req_.first_child();
        for (pugi::xml_node child_node = create_node.first_child(); child_node; child_node = child_node.next_sibling()){
            if (child_node.name() == std::string("account")){
                db_handler dbh_;
                std::string id = child_node.attribute("id").value();
                std::string balance = child_node.attribute("balance").value();
                if (id.empty() || balance.empty()){
                    invalid_request_error();
                    continue;
                }
                Query_Result qr = dbh_.create_account(id, balance);
                if (qr == Query_Result::ACCOUNT_ALREADY_EXIST){
                    account_exist_error(id);
                }
                else if (qr == Query_Result::DATABASE_ERROR){
                    database_error();
                }
                else {
                    response_create_account(id);
                }
            }
            else if (child_node.name() == std::string("symbol")){
                std::string sym = child_node.attribute("sym").value();
                if (sym.empty()){
                    invalid_request_error();
                    continue;
                }
                for (pugi::xml_node account_node = child_node.first_child(); account_node; account_node = account_node.next_sibling()){
                    db_handler dbh_;
                    std::string id = account_node.attribute("id").value();
                    std::string num = account_node.text().get();
                    if (id.empty() || num.empty() || account_node.name() != std::string("account")){
                        invalid_request_error();
                        continue;
                    }
                    Query_Result qr = dbh_.create_sym(id, num, sym);
                    if (qr == Query_Result::ACCOUNT_NOT_EXIST){
                        account_does_not_exist_error(id);
                    }
                    else if (qr == Query_Result::DATABASE_ERROR){
                        database_error();
                    }
                    else {
                        response_create_sym(id, sym);
                    }
                }
            }
            else {
                invalid_request_error();
            }
        }
        return xml_to_string(res_);
    }
    std::string transactions(){
        pugi::xml_node transactions_node = req_.first_child();
        std::string id = transactions_node.attribute("id").value();
        if (id.empty()){
            std::cout<<"empty id\n";
            return invalid_request_response();
        }
        for (pugi::xml_node child_node = transactions_node.first_child(); child_node; child_node = child_node.next_sibling()){
            if (child_node.name() == std::string("order")){
                db_handler dbh_;
                std::string sym = child_node.attribute("sym").value();
                std::string amount = child_node.attribute("amount").value();
                std::string limit = child_node.attribute("limit").value();
                if (sym.empty() || amount.empty() || limit.empty()){
                    std::cerr<<"empty sym, amount, or limit\n";
                    invalid_request_error();
                    continue;
                }
                Query_Result qr = dbh_.order(id, sym, amount, limit, result_node);
                if (qr == Query_Result::ACCOUNT_NOT_EXIST){
                    account_does_not_exist_error(id);
                }
                else if (qr == Query_Result::NOT_ENOUGH_BALANCE){
                    not_enough_balance_error(id);
                }
                else if (qr == Query_Result::NOT_ENOUGH_SYM){
                    not_enough_sym_error(id);
                }
                else if (qr == Query_Result::DATABASE_ERROR){
                    database_error();
                }
                else {
                    
                }
            }
            else if (child_node.name() == std::string("query")){
                db_handler dbh_;
                std::string tran_id = child_node.attribute("id").value();
                if (tran_id.empty()){
                    std::cerr<<"empty tran_id\n";
                    invalid_request_error();
                    continue;
                }
                Query_Result qr = dbh_.status(id, tran_id);
                if (qr == Query_Result::ACCOUNT_NOT_EXIST){
                    account_does_not_exist_error(id);
                }
                else if (qr == Query_Result::TRANSACTION_NOT_FOUND){
                    transaction_does_not_exist_error(tran_id);
                }
                else if (qr == Query_Result::DATABASE_ERROR){
                    database_error();
                }
                else {
                    response_query(id, tran_id, dbh_);
                }
            }
            else if (child_node.name() == std::string("cancel")){
                db_handler dbh_;
                std::string tran_id = child_node.attribute("id").value();
                if (tran_id.empty()){
                    std::cerr<<"empty tran_id\n";
                    invalid_request_error();
                    continue;
                }
                Query_Result qr = dbh_.cancel(id, tran_id);
                if (qr == Query_Result::ACCOUNT_NOT_EXIST){
                    account_does_not_exist_error(id);
                }
                else if (qr == Query_Result::TRANSACTION_NOT_FOUND){
                    transaction_does_not_exist_error(tran_id);
                }
                else if (qr == Query_Result::DATABASE_ERROR){
                    database_error();
                }
                else {
                    response_cancel(id, tran_id, dbh_);
                }
            }
            else {
                std::cout<<"other!!!"<<std::endl;
                invalid_request_error();
            }
        }
        return xml_to_string(res_);
    }
    void response_cancel(std::string &id, std::string &tran_id, db_handler &dbh_){
        // create a cancel node and append it to the response
        pugi::xml_node cancel_node = result_node.append_child("canceled");
        cancel_node.append_attribute("id").set_value(tran_id.c_str());
        dbh_.do_cancel(id, tran_id, cancel_node);
    }
    void response_query(std::string &id, std::string &tran_id, db_handler &dbh_){
        // create a query node and append it to the response
        pugi::xml_node status_node = result_node.append_child("status");
        status_node.append_attribute("id").set_value(tran_id.c_str());
        dbh_.add_status(id, tran_id, status_node);
    }
    void response_create_account(std::string &id){
        // create a created node and append it to the response
        pugi::xml_node create_node = result_node.append_child("created");
        create_node.append_attribute("id").set_value(id.c_str());
    }
    void response_create_sym(std::string &id, std::string &sym){
        // create a created node and append it to the response
        pugi::xml_node create_node = result_node.append_child("created");
        create_node.append_attribute("sym").set_value(sym.c_str());
        create_node.append_attribute("id").set_value(id.c_str());
    }
    void database_error(){
        // create an error node and append it to the response
        pugi::xml_node error_node = result_node.append_child("error");
        error_node.text().set("database error");
    }
    void not_enough_balance_error(std::string &id){
        // create an error node and append it to the response
        pugi::xml_node error_node = result_node.append_child("error");
        error_node.append_attribute("id").set_value(id.c_str());
        error_node.text().set("you do not have enough balance");
    }
    void transaction_does_not_exist_error(std::string &tran_id){
        // create an error node and append it to the response
        pugi::xml_node error_node = result_node.append_child("error");
        error_node.append_attribute("id").set_value(tran_id.c_str());
        error_node.text().set("transaction does not exist");
    }
    void not_enough_sym_error(std::string &id){
        // create an error node and append it to the response
        pugi::xml_node error_node = result_node.append_child("error");
        error_node.append_attribute("id").set_value(id.c_str());
        error_node.text().set("you do not have enough shares");
    }
    void account_does_not_exist_error(std::string &id){
        // create an error node and append it to the response
        pugi::xml_node error_node = result_node.append_child("error");
        error_node.append_attribute("id").set_value(id.c_str());
        error_node.text().set("Create position: account does not exist");
    }

    void account_exist_error(std::string &id){
        // create an error node and append it to the response
        pugi::xml_node error_node = result_node.append_child("error");
        error_node.append_attribute("id").set_value(id.c_str());
        error_node.text().set("Create account: account already exists");
    }

    void invalid_request_error(){
        // create an error node and append it to the response
        pugi::xml_node error_node = result_node.append_child("error");
        error_node.text().set("malformed request");
    }
    std::string invalid_request_response(){
        invalid_request_error();
        std::stringstream ss;
        res_.save(ss);
        return ss.str();
    }
};


