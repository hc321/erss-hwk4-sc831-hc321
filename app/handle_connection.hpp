// #ifndef HANDLE_CONNECTION
// #define HANDLE_CONNECTION
#pragma once
#include "request_handler.hpp"
#include "utils.hpp"

namespace beast = boost::beast;         // from <boost/beast.hpp>
namespace http = beast::http;           // from <boost/beast/http.hpp>
namespace net = boost::asio;            // from <boost/asio.hpp>
using tcp = boost::asio::ip::tcp;       // from <boost/asio/ip/tcp.hpp>

// Handles an HTTP server connection
class session : public std::enable_shared_from_this<session>
{
    beast::tcp_stream stream_;
    std::array<char, 8192> buffer_;
    http::request<http::string_body> req_;
    // http::response<http::string_body> res_;
    std::mutex & my_mutex_;
    std::string extract_text(const std::string & text){
        // extract the size and the rest of the content
        std::stringstream ss(text);
        std::size_t xml_size;
        ss >> xml_size;
        // read the rest of the content of the size into a string 
        std::string xml_content;
        ss.ignore();
        std::getline(ss, xml_content, '\0');
        std::string xml_text = xml_content.substr(0, xml_size);
        return xml_text;
    }
public:
    // Take ownership of the stream
    session(
        tcp::socket&& socket,
        std::mutex & my_mutex)
        : stream_(std::move(socket))
        , my_mutex_(my_mutex)
    {
    }

    // Start the asynchronous operation
    void
    run()
    {
        // We need to be executing within a strand to perform async operations
        // on the I/O objects in this session. Although not strictly necessary
        // for single-threaded contexts, this example code is written to be
        // thread-safe by default.
        net::dispatch(stream_.get_executor(),
                      beast::bind_front_handler(
                          &session::do_read,
                          shared_from_this()));
    }

    void
    do_read()
    {
        boost::system::error_code error;
        while (1){
            // Make the request empty before reading,
            // otherwise the operation behavior is undefined.
            req_ = {};
            // Set the timeout.
            stream_.expires_after(std::chrono::seconds(30));
            // Read a request  
            std::size_t bytes_read = stream_.read_some(boost::asio::buffer(buffer_), error);
            if (error == boost::asio::error::eof){
                stream_.socket().shutdown(tcp::socket::shutdown_send, error);
                break; // Connection closed by peer.
            }            
            else if (error){
                throw boost::system::system_error(error); // Some other error.
            }
            write_response(error, bytes_read);
        }
    }

    void write_response(boost::system::error_code & error, std::size_t bytes_read) {
        if (!error) {
            std::string xml_text(buffer_.data(), bytes_read);
            // Process the received XML text here
            std::string extracted_text = extract_text(xml_text);
            request_handler rh(extracted_text, my_mutex_);
            // Write the response
            std::string res_body = rh.generate_response();
            boost::asio::write(stream_, boost::asio::buffer(res_body), error);
        }
        // shutdown if the client shuts down the connection
        // otherwise, continue reading
        if (error == boost::asio::error::eof)
            stream_.socket().shutdown(tcp::socket::shutdown_send, error);
        if (error)
            throw boost::system::system_error(error);
        else
            do_read();
    }
};

// Accepts incoming connections and launches the sessions
class listener : public std::enable_shared_from_this<listener>
{
    net::io_context& ioc_;
    tcp::acceptor acceptor_;
    std::mutex & my_mutex_;
public:
    listener(
        net::io_context& ioc,
        tcp::endpoint endpoint,
        std::mutex & my_mutex)
        : ioc_(ioc)
        , acceptor_(net::make_strand(ioc))
        , my_mutex_(my_mutex)
    {
        beast::error_code ec;

        // Open the acceptor
        acceptor_.open(endpoint.protocol(), ec);
        if(ec)
        {
            fail(ec, "open");
            return;
        }

        // Allow address reuse
        acceptor_.set_option(net::socket_base::reuse_address(true), ec);
        if(ec)
        {
            fail(ec, "set_option");
            return;
        }

        // Bind to the server address
        acceptor_.bind(endpoint, ec);
        if(ec)
        {
            fail(ec, "bind");
            return;
        }

        // Start listening for connections
        acceptor_.listen(
            net::socket_base::max_listen_connections, ec);
        if(ec)
        {
            fail(ec, "listen");
            return;
        }
    }

    // Start accepting incoming connections
    void
    run()
    {
        do_accept();
    }

private:
    void
    do_accept()
    {
        // The new connection gets its own strand
        acceptor_.async_accept(
            net::make_strand(ioc_),
            beast::bind_front_handler(
                &listener::on_accept,
                shared_from_this()));
    }

    void
    on_accept(beast::error_code ec, tcp::socket socket)
    {
        if(ec)
        {
            fail(ec, "accept");
            return; // To avoid infinite loop
        }
        else
        {
            // Create the session and run it
            std::make_shared<session>(
                std::move(socket),
                my_mutex_)->run();
        }

        // Accept another connection
        do_accept();
    }
};


// #endif