#pragma once
#include "utils.hpp"

enum class Query_Result {
    SUCCESS,
    ACCOUNT_ALREADY_EXIST,
    ACCOUNT_NOT_EXIST,
    NOT_ENOUGH_BALANCE,
    NOT_ENOUGH_SYM,
    TRANSACTION_NOT_FOUND,
    DATABASE_ERROR,
};
// create a db_handler class
class db_handler
{
private:
    pqxx::connection *C;
    pqxx::work *W;

public:
    db_handler(){
        C = new pqxx::connection("host=db port=5432 dbname=STOCK_EXCHANGING_MATCHING user=postgres password=passw0rd");
        W = new pqxx::transaction<>(*C);
        W->exec0("BEGIN ISOLATION LEVEL SERIALIZABLE");
    }; // default constructor
    // destructor
    ~db_handler(){
        delete W;
        C->disconnect();
        delete C;
    }
    Query_Result create_account(std::string &id, std::string &balance){
        // create a new account with id and balance
        // return ACCOUNT_ALREADY_EXIST if there is already an account with the same id
        try {            
            std::string check_query = "SELECT account_id FROM account WHERE account_id = " + id + ";";
            pqxx::result check_result = W->exec(check_query);
            if (check_result.empty()){
                std::string insert_query = "INSERT INTO account VALUES (" + id + ", " + balance + ");";
                W->exec(insert_query);
            }
            else{
                return Query_Result::ACCOUNT_ALREADY_EXIST;
            }            
        } catch (const std::exception &e) {
            return Query_Result::DATABASE_ERROR;
        }
        W->commit();
        return Query_Result::SUCCESS;
    }
    Query_Result create_sym(std::string &id, std::string &num, std::string &sym){
        // create a new sym with sym, id, and num
        // return ACCOUNT_NOT_EXIST if there is no account with the id
        try {
            std::string check_query = "SELECT account_id FROM account WHERE account_id = " + id + ";";
            pqxx::result check_result = W->exec(check_query);
            if (check_result.empty()){
                return Query_Result::ACCOUNT_NOT_EXIST;
            }
            else{
                // insert into sym if the sym does not exist
                std::string check_symbol_query = "SELECT symbol_name FROM symbol WHERE symbol_name = " + 
                    W->quote(sym) + ";";
                pqxx::result check_symbol_result = W->exec(check_symbol_query);
                if (check_symbol_result.empty()){
                    std::string insert_query = "INSERT INTO symbol (symbol_name) VALUES (" + W->quote(sym) + ");";
                    W->exec0(insert_query);
                }
                // Insert the record into the POSITION table with the given account_id, symbol_name, and share_amount
                pqxx::result check_position_result = W->exec("SELECT * FROM position WHERE account_id = " + id + 
                    " AND symbol_name = " + W->quote(sym) + ";");
                // if the position already exists, update the share_amount with the current amount + num
                if (!check_position_result.empty()){
                    std::string update_position_query = "UPDATE position SET share_amount = share_amount + " + num + 
                        " WHERE account_id = " + id + " AND symbol_name = " + W->quote(sym) + ";";
                    W->exec0(update_position_query);
                }
                else {
                    std::string insert_position_query = "INSERT INTO position (account_id, symbol_name, share_amount) VALUES (" + 
                        id + ", " + W->quote(sym) + ", " + num + ")";
                    W->exec0(insert_position_query);
                }                
                W->commit();
            }
        } catch (const std::exception &e) {
            //std::lock_guard<std::mutex> lock(log_mutex);
            std::cerr << e.what() << std::endl;
            return Query_Result::DATABASE_ERROR;
        }
        return Query_Result::SUCCESS;
    }
    Query_Result order(std::string &id, std::string &sym, std::string &amount, std::string &limit, pugi::xml_node &child_node){
        // create a new order with id, sym, amount, and limit
        // return SUCCESS if the order is successfully created
        // return ACCOUNT_NOT_EXIST if there is no account with the id
        // return NOT_ENOUGH_BALANCE if the account does not have enough balance
        try {
            std::string actual_amount = amount;
            std::string check_account_query = "SELECT account_id FROM account WHERE account_id = " + id + ";";
            pqxx::result check_account_result = W->exec(check_account_query);
            if (check_account_result.empty()){
                return Query_Result::ACCOUNT_NOT_EXIST;
            } 
            // check if the account has enough balance
            std::string check_balance_query = "SELECT balance FROM account WHERE account_id = " + id + ";";            
            pqxx::result check_balance_result = W->exec(check_balance_query);
            double balance = check_balance_result[0]["balance"].as<double>();
            double amount_num = std::stod(amount);
            double limit_num = std::stod(limit);  
            // check if the seller has enought amount of symbols
            if (amount_num < 0){
                std::string check_position_query = "SELECT * FROM position WHERE account_id = " + id + 
                    " AND symbol_name = " + W->quote(sym) + ";";
                pqxx::result check_position_result = W->exec(check_position_query);                    
                if (check_position_result.empty() ){
                    return Query_Result::NOT_ENOUGH_SYM;
                }
                double share_amount = check_position_result[0]["share_amount"].as<double>();
                if (share_amount < -amount_num){
                    return Query_Result::NOT_ENOUGH_SYM;
                }     
            }                
            if (amount_num > 0 && balance < amount_num * limit_num){
                return Query_Result::NOT_ENOUGH_BALANCE;
            }
            else {
                // Insert symbol_name into the symbol table if it doesn't exist
                std::string insert_symbol_query = "INSERT INTO symbol (symbol_name) VALUES (" + W->quote(sym) + 
                    ") ON CONFLICT (symbol_name) DO NOTHING;";
                W->exec0(insert_symbol_query);
                // create an order in the orders table
                std::string insert_order_query = "INSERT INTO orders (account_id, symbol_name, \
                    limited_price, amount, stat, order_time) VALUES (" + 
                    id + ", " + W->quote(sym)+ ", " + limit + ", " + amount + 
                    ", 'open', (EXTRACT(EPOCH FROM now()) * 1000)::BIGINT) \
                    RETURNING order_id, transaction_id;";
                // get the order_id and transaction_id of the order I just inserted
                pqxx::result insert_order_result = W->exec(insert_order_query);
                std::string order_id = insert_order_result[0]["order_id"].as<std::string>();
                std::string transaction_id = insert_order_result[0]["transaction_id"].as<std::string>();
                if (amount_num > 0){
                    // make buy order, subtract the balance
                    std::string update_balance_query = "UPDATE account SET balance = balance - " + 
                        std::to_string(amount_num * limit_num) + " WHERE account_id = " + id + ";";
                    W->exec0(update_balance_query);
                    // match the buy order with sell orders in the orders table
                    std::string match_query = "SELECT * FROM orders WHERE symbol_name = " + W->quote(sym) + 
                        " AND limited_price <= " + limit + " AND stat = 'open'" +
                        " AND amount < 0 ORDER BY limited_price ASC, order_time ASC;";
                    pqxx::result match_result = W->exec(match_query);
                    for (auto row: match_result){
                        if (amount_num == 0){
                            break;
                        }
                        std::string match_order_id = row["transaction_id"].as<std::string>();
                        std::string match_id = row["account_id"].as<std::string>();
                        std::string match_amount = row["amount"].as<std::string>();
                        double match_amount_num = -std::stod(match_amount);
                        double trade_amount = std::min(amount_num, match_amount_num);
                        std::string match_limit = row["limited_price"].as<std::string>();
                        double match_limit_num = std::stod(match_limit);                        
                        if (amount_num == match_amount_num){
                            std::string update_order_query = "UPDATE orders SET stat = 'executed' \
                                WHERE order_id = " + order_id  + ";";   
                            W->exec0(update_order_query);
                            std::string update_sell_order_query = "UPDATE orders SET stat = 'executed' \
                                WHERE order_id = " + row["order_id"].as<std::string>() + ";";
                            W->exec0(update_sell_order_query);
                        }
                        // if the buy order is partially matched
                        else if (amount_num > match_amount_num){
                            std::string update_order_query = "UPDATE orders SET amount = amount - " + 
                                std::to_string(trade_amount) + " WHERE order_id = " + order_id + ";";
                            W->exec0(update_order_query);
                            std::string update_sell_order_query = "UPDATE orders SET stat = 'executed' \
                                WHERE order_id = " + row["order_id"].as<std::string>() + ";";
                            W->exec0(update_sell_order_query);                            
                        }
                        // if the sell order is fully matched
                        else {                            
                            std::string update_sell_order_query = "UPDATE orders SET amount = amount - " + 
                                std::to_string(trade_amount) + " WHERE order_id = " + row["order_id"].as<std::string>() + ";";
                            W->exec0(update_sell_order_query); 
                            std::string update_order_query = "UPDATE orders SET stat = 'executed' \
                                WHERE order_id = " + order_id  + ";";   
                            W->exec0(update_order_query);  
                        }
                        amount_num -= trade_amount;                        
                        // update the executed table
                        std::string insert_executed_query_buy = "INSERT INTO executed_order (executed_order_id, account_id, \
                            symbol_name, share_amount, price, executed_time) VALUES (" + transaction_id + ", " + id + ", " + 
                            W->quote(sym)+ ", " + std::to_string(trade_amount) + ", " + 
                            match_limit + ", (EXTRACT(EPOCH FROM now()) * 1000)::BIGINT);";
                        W->exec0(insert_executed_query_buy);
                        std::string insert_executed_query_sell = "INSERT INTO executed_order (executed_order_id, account_id, \
                            symbol_name, share_amount, price, executed_time) VALUES (" + match_order_id + ", " + match_id + ", " + 
                            W->quote(sym)+ ", " + std::to_string(-trade_amount) + ", " + 
                            match_limit + ", (EXTRACT(EPOCH FROM now()) * 1000)::BIGINT);";
                        W->exec0(insert_executed_query_sell);
                        // update the balance of the buyer
                        double return_price = trade_amount * (limit_num - match_limit_num);
                        std::string update_buy_balance_query = "UPDATE account SET balance = balance + " + 
                            std::to_string(return_price) + " WHERE account_id = " + id + ";";
                        W->exec0(update_buy_balance_query);
                        // add the amount to buyer's position
                        std::string buy_position_query = "SELECT * FROM position WHERE account_id = " + id + 
                            " AND symbol_name = " + W->quote(sym) + ";";
                        pqxx::result buy_position_result = W->exec(buy_position_query);
                        if (buy_position_result.empty()){
                            std::string insert_buy_position_query = "INSERT INTO position (account_id, symbol_name, share_amount) \
                                VALUES (" + id + ", " + W->quote(sym) + ", " + std::to_string(trade_amount) + ");";
                            W->exec0(insert_buy_position_query);
                        }
                        else{
                            std::string update_buy_position_query = "UPDATE position SET share_amount = share_amount + " + 
                                std::to_string(trade_amount) + " WHERE account_id = " + id + 
                                " AND symbol_name = " + W->quote(sym) + ";";
                            W->exec0(update_buy_position_query);
                        }                  
                        // update the balance and position of the seller
                        std::string update_sell_balance_query = "UPDATE account SET balance = balance + " + 
                            std::to_string(trade_amount * std::stod(match_limit)) + " WHERE account_id = " + 
                            match_id + ";";
                        W->exec0(update_sell_balance_query);
                        // subtract the share_amount from the position of the seller, 
                        // delete the position if the share_amount is 0
                        std::string update_position_query = "UPDATE position SET share_amount = share_amount - " + 
                            std::to_string(trade_amount) + " WHERE account_id = " + match_id + 
                            " AND symbol_name = " + W->quote(sym) + " RETURNING share_amount;";
                        pqxx::result update_position_result = W->exec(update_position_query);
                        double share_amount = update_position_result[0]["share_amount"].as<double>();
                        if (share_amount == 0){
                            std::string delete_position_query = "DELETE FROM position WHERE account_id = " + 
                                match_id + " AND symbol_name = " + W->quote(sym) + ";";
                            W->exec0(delete_position_query);
                        }
                    }
                }   
                else if (amount_num < 0){
                    amount_num = -amount_num;                    
                    // match the sell order with buy orders in the orders table                
                    std::string match_query = "SELECT * FROM orders WHERE symbol_name = " + W->quote(sym) + 
                        " AND limited_price >= " + limit + " AND stat = 'open'" + 
                        " AND amount > 0 ORDER BY limited_price DESC, order_time ASC;";
                    pqxx::result match_result = W->exec(match_query);
                    for (auto row: match_result){
                        if (amount_num == 0){
                            break;
                        }
                        std::string match_order_id = row["transaction_id"].as<std::string>();
                        std::string match_id = row["account_id"].as<std::string>();
                        std::string match_amount = row["amount"].as<std::string>();
                        double match_amount_num = std::stod(match_amount);
                        double trade_amount = std::min(amount_num, match_amount_num);
                        std::string match_limit = row["limited_price"].as<std::string>();
                        double match_limit_num = std::stod(match_limit);                        
                        if (amount_num == match_amount_num){
                            std::string update_order_query = "UPDATE orders SET stat = 'executed' \
                                WHERE order_id = " + order_id  + ";";   
                            W->exec0(update_order_query);
                            std::string update_sell_order_query = "UPDATE orders SET stat = 'executed' \
                                WHERE order_id = " + row["order_id"].as<std::string>() + ";";
                            W->exec0(update_sell_order_query);
                        }
                        // if the sell order is partially matched
                        else if (amount_num > match_amount_num){
                            std::string update_order_query = "UPDATE orders SET amount = amount - " + 
                                std::to_string(trade_amount) + " WHERE order_id = " + order_id + ";";
                            W->exec0(update_order_query);
                            std::string update_buy_order_query = "UPDATE orders SET stat = 'executed' \
                                WHERE order_id = " + row["order_id"].as<std::string>() + ";";
                            W->exec0(update_buy_order_query);                            
                        }
                        // if the sell order is fully matched
                        else {                            
                            std::string update_buy_order_query = "UPDATE orders SET amount = amount - " + 
                                std::to_string(trade_amount) + " WHERE order_id = " + row["order_id"].as<std::string>() + ";";
                            W->exec0(update_buy_order_query); 
                            std::string update_order_query = "UPDATE orders SET stat = 'executed' \
                                WHERE order_id = " + order_id  + ";";   
                            W->exec0(update_order_query);  
                        }
                        amount_num -= trade_amount; 
                        // update the executed table
                        std::string insert_executed_query_sell = "INSERT INTO executed_order (executed_order_id, account_id, \
                            symbol_name, share_amount, price, executed_time) VALUES (" + transaction_id + ", " + id + ", " + 
                            W->quote(sym)+ ", " + std::to_string(-trade_amount) + ", " + 
                            match_limit + ", (EXTRACT(EPOCH FROM now()) * 1000)::BIGINT);";
                        W->exec0(insert_executed_query_sell);
                        std::string insert_executed_query_buy = "INSERT INTO executed_order (executed_order_id, account_id, \
                            symbol_name, share_amount, price, executed_time) VALUES (" + match_order_id + ", " + match_id + ", " + 
                            W->quote(sym)+ ", " + std::to_string(trade_amount) + ", " + 
                            match_limit + ", (EXTRACT(EPOCH FROM now()) * 1000)::BIGINT);";
                        W->exec0(insert_executed_query_buy);
                        // update the balance of the buyer
                        double return_price = trade_amount * (limit_num - match_limit_num);
                        std::string update_buy_balance_query = "UPDATE account SET balance = balance + " + 
                            std::to_string(return_price) + " WHERE account_id = " + match_id + ";";
                        W->exec0(update_buy_balance_query);
                        // add the amount to buyer's position
                        std::string buy_position_query = "SELECT * FROM position WHERE account_id = " + match_id + 
                            " AND symbol_name = " + W->quote(sym) + ";";
                        pqxx::result buy_position_result = W->exec(buy_position_query);
                        if (buy_position_result.empty()){
                            std::string insert_buy_position_query = "INSERT INTO position (account_id, symbol_name, share_amount) \
                                VALUES (" + match_id + ", " + W->quote(sym) + ", " + std::to_string(trade_amount) + ");";
                            W->exec0(insert_buy_position_query);
                        }
                        else{
                            std::string update_buy_position_query = "UPDATE position SET share_amount = share_amount + " + 
                                std::to_string(trade_amount) + " WHERE account_id = " + match_id + 
                                " AND symbol_name = " + W->quote(sym) + ";";
                            W->exec0(update_buy_position_query);
                        }  
                        // update the balance and position of the seller
                        std::string update_sell_balance_query = "UPDATE account SET balance = balance + " + 
                            std::to_string(trade_amount * std::stod(match_limit)) + " WHERE account_id = " + 
                            id + ";";
                        W->exec0(update_sell_balance_query);
                        // subtract the share_amount from the position of the seller, 
                        // delete the position if the share_amount is 0
                        std::string update_position_query = "UPDATE position SET share_amount = share_amount - " + 
                            std::to_string(trade_amount) + " WHERE account_id = " + id + 
                            " AND symbol_name = " + W->quote(sym) + " RETURNING share_amount;";
                        pqxx::result update_position_result = W->exec(update_position_query);
                        double share_amount = update_position_result[0]["share_amount"].as<double>();
                        if (share_amount == 0){
                            std::string delete_position_query = "DELETE FROM position WHERE account_id = " + 
                                match_id + " AND symbol_name = " + W->quote(sym) + ";";
                            W->exec0(delete_position_query);
                        }
                    }
                }   
                else {
                    return Query_Result::DATABASE_ERROR;
                }
                child_node.append_attribute("sym").set_value(sym.c_str());
                child_node.append_attribute("amount").set_value(actual_amount.c_str());
                child_node.append_attribute("limit").set_value(limit.c_str()); 
                child_node.append_attribute("id").set_value(transaction_id.c_str());         
            }

            W->commit();
        } catch (const std::exception &e) {
            std::cerr<<e.what()<<std::endl;
            return Query_Result::DATABASE_ERROR;
        }
        return Query_Result::SUCCESS; 
    }
    Query_Result status(std::string &id, std::string &tran_id){
        // return the status of the order with the tran_id and account id
        // return ACCOUNT_NOT_EXIST if there is no account with the id
        // return TRANSACTION_NOT_FOUND if there is no transaction with the tran_id and id
        try {
            std::string check_account_query = "SELECT account_id FROM account WHERE account_id = " + id + ";";
            pqxx::result check_account_result = W->exec(check_account_query);
            if (check_account_query.empty()){
                return Query_Result::ACCOUNT_NOT_EXIST;
            }
            std::string check_transaction_query = "SELECT * FROM orders WHERE transaction_id = " + tran_id + 
                " AND account_id = " + id + ";";
            pqxx::result check_transaction_result = W->exec(check_transaction_query);
            if (check_transaction_result.empty()){
                return Query_Result::TRANSACTION_NOT_FOUND;
            }
        } catch (const std::exception &e) {
            return Query_Result::DATABASE_ERROR;
        }
        return Query_Result::SUCCESS;
    }
    void add_status(std::string &id, std::string &tran_id, pugi::xml_node &status_node){
        // query the database for the status of the order with the tran_id and account id
        // add the status of the order with the tran_id and account id to the status_node
        try {
            // query in the orders table for the open/canceled status
            std::string check_open_cancel = "SELECT * FROM orders WHERE transaction_id = " + tran_id + 
                " AND account_id = " + id + " AND stat = 'open' OR stat = 'canceled';";
            pqxx::result check_open_cancel_result = W->exec(check_open_cancel);
            if (!check_open_cancel_result.empty()){
                if (check_open_cancel_result[0]["stat"].as<std::string>() == "open"){
                    pugi::xml_node open_node = status_node.append_child("open");
                    open_node.append_attribute("shares").
                        set_value(check_open_cancel_result[0]["amount"].as<std::string>().c_str());
                }
                else {
                    pugi::xml_node cancel_node = status_node.append_child("canceled");
                    cancel_node.append_attribute("shares").
                        set_value(check_open_cancel_result[0]["amount"].as<std::string>().c_str());
                    // query canceled table for the canceled time
                    std::string check_canceled = "SELECT * FROM canceled_order WHERE canceled_order_id = " + tran_id + ";";
                    pqxx::result check_canceled_result = W->exec(check_canceled);
                    if (!check_canceled_result.empty()){
                        cancel_node.append_attribute("time").
                            set_value(check_canceled_result[0]["canceled_time"].as<std::string>().c_str());
                    }                    
                }
            }
            // query executed_order for the information
            std::string check_executed = "SELECT * FROM executed_order WHERE executed_order_id = " + 
                    tran_id + " AND account_id = " + id + "ORDER BY executed_time DESC;";
            pqxx::result check_executed_result = W->exec(check_executed);
            if (!check_executed_result.empty()){
                for (std::size_t i = 0; i < check_executed_result.size(); i++){
                    pugi::xml_node child_node = status_node.append_child("executed");
                    child_node.append_attribute("shares").
                        set_value(check_executed_result[i]["share_amount"].as<std::string>().c_str());
                    child_node.append_attribute("price").
                        set_value(check_executed_result[i]["price"].as<std::string>().c_str());
                    child_node.append_attribute("time").
                        set_value(check_executed_result[i]["executed_time"].as<std::string>().c_str());
                }              
            }
        } catch (const std::exception &e) {
            std::cerr<<e.what()<<std::endl;
        }
        return;
    }
    Query_Result cancel(std::string &id, std::string &tran_id){
        // cancel the order with the tran_id and account id
        // return ACCOUNT_NOT_EXIST if there is no account with the id
        // return TRANSACTION_NOT_FOUND if there is no transaction with the tran_id and id
        try {
            std::string check_account_query = "SELECT account_id FROM account WHERE account_id = " + id + ";";
            pqxx::result check_account_result = W->exec(check_account_query);
            if (check_account_query.empty()){
                return Query_Result::ACCOUNT_NOT_EXIST;
            }
            std::string check_transaction_query = "SELECT * FROM orders WHERE transaction_id = " + tran_id + 
                " AND account_id = " + id + " AND stat = 'open';";
            pqxx::result check_transaction_result = W->exec(check_transaction_query);
            if (check_transaction_result.empty()){
                return Query_Result::TRANSACTION_NOT_FOUND;
            }
        } catch (const std::exception &e) {
            return Query_Result::DATABASE_ERROR;
        }
        return Query_Result::SUCCESS;
    }
    void do_cancel(std::string &id, std::string &tran_id, pugi::xml_node &cancel_node){
        // cancel the order with the tran_id and account id
        // add the result to the cancel_node
        try {
            // query the orders table 
            std::string check_order_query = "SELECT * FROM orders WHERE transaction_id = " + tran_id + 
                " AND account_id = " + id + " AND stat = 'open';";
            pqxx::result check_order_result = W->exec(check_order_query);
            pugi::xml_node cancel_child = cancel_node.append_child("canceled");
            double amount = check_order_result[0]["amount"].as<double>();
            if (amount > 0){
                double price = check_order_result[0]["price"].as<double>();
                double return_amount = price * amount;
                // update balance in account table
                std::string update_balance = "UPDATE account SET balance = balance + " + 
                    std::to_string(return_amount) + " WHERE account_id = " + id + ";";
                W->exec(update_balance);               
            }
            // update the canceled_order table with the query result
            std::string update_canceled_order = "INSERT INTO canceled_order (canceled_order_id, canceled_time) VALUES (" + 
                tran_id  + ", (EXTRACT(EPOCH FROM now()) * 1000)::BIGINT)";
            W->exec0(update_canceled_order);
            // update the record in the orders table with the query result
            std::string update_order = "UPDATE orders SET stat = 'canceled' WHERE transaction_id = " + 
                tran_id + " AND stat = 'open';";
            W->exec0(update_order);
            cancel_child.append_attribute("shares").set_value(amount);
            // query canceled table for the canceled time
            std::string check_canceled = "SELECT * FROM canceled_order WHERE canceled_order_id = " + tran_id + ";";
            pqxx::result check_canceled_result = W->exec(check_canceled);
            cancel_child.append_attribute("time").set_value(check_canceled_result[0]["canceled_time"].as<std::string>().c_str());
            // query executed table for the executed time
            std::string check_executed = "SELECT * FROM executed_order WHERE executed_order_id = " + 
                    tran_id + " AND account_id = " + id + "ORDER BY executed_time DESC;";
            pqxx::result check_executed_result = W->exec(check_executed);
            if (!check_executed_result.empty()){
                for (std::size_t i = 0; i < check_executed_result.size(); i++){
                    pugi::xml_node child_node = cancel_node.append_child("executed");
                    child_node.append_attribute("shares").
                        set_value(check_executed_result[i]["share_amount"].as<std::string>().c_str());
                    child_node.append_attribute("price").
                        set_value(check_executed_result[i]["price"].as<std::string>().c_str());
                    child_node.append_attribute("time").
                        set_value(check_executed_result[i]["executed_time"].as<std::string>().c_str());
                }              
            }
            W->commit();
        } catch (const std::exception &e) {
            std::cerr<<e.what()<<std::endl;
        }
        return;
    }
};