1. The buffer in the session class may be deployed to make buffer overflow attack.
2. ACCOUNT_ALREADY_EXIST:
This error occurs when you try to create an account with an ID that already exists in the database.
3. ACCOUNT_NOT_EXIST:
This error occurs when you try to create a new symbol for an account ID that does not exist in the database.
4. DATABASE_ERROR:
This error occurs when there is an issue with the database operation, such as a query syntax error or a connection problem.
5. NOT_ENOUGH_BALANCE:
This error occurs when the buyer sets an order when she does not owns enough balance in her account.
6. NOT_ENOUGH_SYM:
This error occurs when the seller sets an order when she does not owns enough shares in her position.